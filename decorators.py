# -*- coding: utf-8 -*-
import json
from functools import wraps, update_wrapper
from datetime import timedelta
from flask import make_response, current_app, session, request, redirect, url_for

def auth_required(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        if 'auth' not in session:
            return json.dumps({"error" : "Unauthorized"}), 401
        if not session['auth'] and not session['admin']:
            return json.dumps({"error" : "Unauthorized"}), 401    
        return func(*args, **kwargs)
    return decorated


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'auth' in session:
            if session['auth'] and session['admin']:
                return f(*args, **kwargs)   
        return redirect(url_for('login'))
    return decorated_function

def json_response(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if True and request.method == 'OPTIONS':
            resp = current_app.make_default_options_response()
        else:
            resp = make_response(f(*args, **kwargs))
        if not True and request.method != 'OPTIONS':
            return resp

        h = resp.headers

        h['Access-Control-Allow-Origin'] = '*'
        # h['Access-Control-Allow-Origin'] = origin
        # h['Access-Control-Allow-Methods'] = None
        h['Access-Control-Max-Age'] = str(21600)
        h['Content-type'] = "application/json"
        
        #if headers is not None:
        #    h['Access-Control-Allow-Headers'] = None
        return resp
    return decorated_function
