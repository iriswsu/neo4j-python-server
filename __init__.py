from decorators import json_response
from flask import Flask, request, redirect, url_for, render_template
import requests
import dataset
import json
import re

db = dataset.connect('sqlite:///:memory:')

API_USERNAME = "k2jfo2JMM--123n2kadleEkaknv"
API_PASSWORD = "92nnL2liemN2K009Nilek81lfFl"

# may want to just load the data neo4j databse

app = Flask(__name__)

@app.route('/')
@json_response
def index():
	return redirect(url_for('docs'))

@app.route('/api/v1/docs', methods=['GET'])
def docs():
	return render_template('tmp.html')

# might just want to keep a database with the locations for easy searching add a post request on creation of nodes in the admin side

@app.route('/api/v1/location', methods=['POST'])
@json_response
def create_location():
	table = db['locations']
	data = request.json
	table.insert(data)
	return "", 201


@app.route('/api/v1/location', methods=['GET'])
@json_response
def get_locations():
	username = request.args.get('username', '')
	password = request.args.get('password', '')

	# If the api call included a valid username and password
	if username == API_USERNAME:
		if password == API_PASSWORD:
			search = request.args.get('q', '')
		        if search:
				# with tags search (might need to be a special option to search with tags) 
				# MATCH (n:Destination) WHERE n.name =~ "(?i).*el.*" OR ANY (x IN n.tags WHERE x =~ "cs") RETURN n
				query = '{"query": "MATCH (n:Destination) WHERE n.name =~ \\\"(?i).*%s.*\\\" OR n.abbrv =~ \\\"(?i).*%s.*\\\" OR ANY (x IN n.tags WHERE x =~ \\\"(?i).*%s.*\\\" ) RETURN { id : ID(n), data : n}"}' % (search, search, search)
		        	y = requests.post('http://127.0.0.1:7474/db/data/cypher', data=query,  headers={"Content-Type" : "application/json"})
				data = y.json()
				result = { "type" : "location", "data" : [{"data" : n[0]['data']['data'], "id" : n[0]['id'], "self" : n[0]['data']['self']} for n in data['data']]}
				return json.dumps(result, indent=4), 200
			
			query = '{"query": "MATCH (n:Destination) RETURN {id : ID(n), data : n} LIMIT 25"}'
		        n = requests.post('http://127.0.0.1:7474/db/data/cypher', data=query,  headers={"content-type":"application/json"})
			data = n.json()
			result = {"type" : "location", "data" : [{"data" : n[0]['data']['data'], "id" : n[0]['id'], "self" : n[0]['data']['self']} for n in data['data']]}
			return json.dumps(result, indent=4), 200
	        #return json.dumps(n.json(), indent=4), 200
	    	else:
	    		return "invalid credentials"
        else:
			return "invalid credentials"


@app.route('/api/v1/destination', methods=['GET'])
@json_response
def get_destination():
	username = request.args.get('username', '')
	password = request.args.get('password', '')

	# If the api call included a valid username and password
	if username == API_USERNAME:
		if password == API_PASSWORD:
			# Get the destination id from the GET variable in the URL
			destination_id = request.args.get('id', '')
			if destination_id:
				try:
					# Query the database for all of the data relevant to the destination with an id of <destination_id>
					query = '{"query": "match (n:Destination) where id(n)=%s return n"}' % (destination_id)
			     		y = requests.post('http://127.0.0.1:7474/db/data/cypher', data=query,  headers={"Content-Type" : "application/json"})
					data = y.json()

					# Trim off the unneeded content and only return the attributes of the destination
					result = data['data'][0][0]['data']

					# Return 'result' as a json object
					return json.dumps(result, indent=4), 200
				except IndexError as e:
					data = { "type": "error", "message" : "Could not locate destination."}
					return json.dumps(data, indent=4), 200
			else:
				return "Must send a GET variable of 'id'"
		else:
			return "invalid credentials"
	else:
		return "invalid credentials"




@app.route('/api/v1/path/<NODE_ID>', methods=['GET'])
@json_response
def get_path(NODE_ID):
	username = request.args.get('username', '')
	password = request.args.get('password', '')

	# If the api call included a valid username and password
	if username == API_USERNAME:
		if password == API_PASSWORD:
			lat = request.args.get('lat', '')
			lng = request.args.get('lng', '')
			options = request.args.get('options', '')

			# going to try and use the spatial neo4js plugins to calculate the closes nodes
			query = '{"query": "START n=node:geom(\'withinDistance:[%s, %s, 10.0]\') RETURN ID(n) LIMIT 1"}' % (lat, lng)
			n = requests.post('http://127.0.0.1:7474/db/data/cypher', data=query,  headers={"content-type":"application/json"})
			data =  n.json()
			print json.dumps(data)

			try:
				start = data['data'][0][0]
			except IndexError as e:
				data = { "type": "error", "message" : "Could not locate nearest waypont."}
				return json.dumps(data, indent=4), 200

			# might need to change this query to make it work better with the perfered routing (disabled, indoor)
			#query = '{"to" : "http://127.0.0.1:7474/db/data/node/%s", "cost_property" : "distance", "algorithm" : "dijkstra"}' % (NODE_ID)
			query = '{"to" : "http://127.0.0.1:7474/db/data/node/%s", "cost_property" : "distance", "algorithm" : "dijkstra"}' % (NODE_ID)
		        r = requests.post ('http://127.0.0.1:7474/db/data/node/%d/path' % (start), data=query, headers={"content-type":"application/json"})
		        data =  r.json();
			print json.dumps(data)

		        data['path'] = []
		        try:
		            for node in data['nodes']:
		                n = requests.get(node, headers={"content-type":"application/json"})
		                result = n.json()
		                data['path'].append(result['data'])
		                #print json.dumps(result['data'], indent=4)
		        except KeyError as a: 
		        	data = {"type": "error", "message": "An error occured calculating the path."}
			data['type'] = "path"
		    	return json.dumps(data, indent=4), 200
	        else:
				return "invalid credentials"
	else:
		return "invalid credentials"


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
