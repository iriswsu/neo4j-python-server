# README #

# What is this repository for? #

* This is the python server that is used for the api requests that are accessed through http://<url_of_VM_server>:5000/api/v1/

#What are the available api methods? #

* All method documentation can be found at: http://<url_of_VM_server>:5000/api/v1/docs

# What files should I edit/improve on this repo? #

* When the VM is instantiated you will need to edit the rootWebUrl variable in /templates/tmp.html in order to display the correct URLs on the docs page
* The docs page is located at: /templates/tmp.html
* The api methods are created through python in: __init__.py

# How do I access the web page hosted by this python server? #

* Server url: http://<url_of_VM_server>:5000   
* The api username is: k2jfo2JMM--123n2kadleEkaknv
* The api password is: 92nnL2liemN2K009Nilek81lfFl
* Note: these credentials are set in __init__.py

# How do I get set up? #

* This repo is already loaded on the VM server that is included in the project.  The python server is setup to automatically start itself when the vm is booted.  If the server needs to be started again for some reason, it can be done either by restarting the server or by using the commands below.

# Where is this repo located on the VM server? #

* It is located at ~/server

### Commands: ###
* start server (be in the ~ directory to run this command. Get there by doing: cd ~): nohup python server/__init__.py &
* Check if python server is running: ps -ef | grep python
* Kill server by doing: kill -9 <PID found using the ps -ef command above>

# Who do I talk to if I have questions about this? #

* Jeff Wenzbauer: jwenz723@gmail.com
* Klint Holmes: klintholmes@weber.edu